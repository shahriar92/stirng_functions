<?php

/*
 * Remove whitespace or other characters from left side of the string
 * string ltrim(string $string)
 */
$str = "\n\n\nHello World!";
echo "Without ltrim: " . $str;
echo "<br>";
echo "With ltrim: " . ltrim($str);
?>
