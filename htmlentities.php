<?php

/*convert all applicable characters to html entities
 * sting htmlentities(string $string)
 */
$str = "A 'quote' is <b>bold</b>";

// Outputs: A 'quote' is &lt;b&gt;bold&lt;/b&gt;
echo htmlentities($str);
?>
